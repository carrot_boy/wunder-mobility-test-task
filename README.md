# Wunder Mobility - test task

## What was done and why was done this way:

- I used the session instead of MySQL/SQLite because of 2 reasons: it's easier, and it's more "GDPR compliant".

## Possible optimizations:

- Use browser native validation. Now the symfony validation is being used, it's quite ok, but some frontend validation is needed.
- Async paymentId retrieval for the final step. It requires extra work for frontend (transform plain pages into SPA with state checking, for example, via socket)

## What I would like to do:

- I'd like to dockerize app. Since it's a simple app without dependencies, I decided not to spend time for dockerising.
- Tests! Shame on me because this small app doesn't have any tests, because mostly lack of the time, but I do like to introduce at least couple of unit tests (for StateManager and SaveHandler) and some functional tests.
- Increase observability of app: add monitoring and logging - essential part of app (if you want it works).
- Separate environment. Now there is only one .env file in the root folder, but I'd like to put more environment dependent variables in the file and split it into dev, test and prod.
- Create a simple CI pipeline with mess detector & running tests
