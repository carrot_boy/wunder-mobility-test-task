<?php

declare(strict_types=1);

namespace App\Controller;

use App\Factory\SignUpFormFactory;
use App\Service\SignUpSaveHandler;
use App\Service\SignUpStateManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class SignUpController extends AbstractController
{
    private SignUpFormFactory $formFactory;

    private SignUpStateManager $stateManager;

    public function __construct(SignUpFormFactory $formFactory, SignUpStateManager $stateManager)
    {
        $this->formFactory = $formFactory;
        $this->stateManager = $stateManager;
    }

    public function generalAction(Request $request): Response
    {
        $route = $request->get('_route');
        if ($this->stateManager->isStepFilled($route)) {
            return $this->redirectToRoute(
                $this->stateManager->getNextPossibleStep($route)
            );
        }

        $form = $this->formFactory->createGeneralForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $this->stateManager->saveState($route, $form->getData());
            $nextStep = $this->stateManager->getNextStep($route);
            return $this->redirectToRoute($nextStep);
        }

        return $this->render('signup/general_info.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function addressAction(Request $request): Response
    {
        $route = $request->get('_route');
        if ($this->stateManager->isStepFilled($route)) {
            return $this->redirectToRoute(
                $this->stateManager->getNextPossibleStep($route)
            );
        }

        $form = $this->formFactory->createAddressForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $this->stateManager->saveState($route, $form->getData());
            $nextStep = $this->stateManager->getNextStep($route);
            return $this->redirectToRoute($nextStep);
        }

        return $this->render('signup/address.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function billingAction(Request $request): Response
    {
        $route = $request->get('_route');
        if ($this->stateManager->isStepFilled($route)) {
            return $this->redirectToRoute(
                $this->stateManager->getNextPossibleStep($route)
            );
        }

        $form = $this->formFactory->createBillingForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $this->stateManager->saveState($route, $form->getData(), SignUpSaveHandler::PAYMENT_ID_KEY);
            $nextStep = $this->stateManager->getNextStep($route);

            return $this->redirectToRoute($nextStep);
        }

        return $this->render('signup/billing_info.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function successAction(Request $request): Response
    {
        return $this->render('signup/success.html.twig', [
            'summary' => $this->stateManager->getSummary(),
        ]);
    }
}