<?php

declare(strict_types=1);

namespace App\Entity;

class AddressSignUpEntity
{
    private int $zipCode;

    private string $city;

    private string $street;

    private int $houseNumber;

    /**
     * @return int
     */
    public function getZipCode(): int
    {
        return $this->zipCode;
    }

    /**
     * @param int $zipCode
     *
     * @return AddressSignUpEntity
     */
    public function setZipCode(int $zipCode): AddressSignUpEntity
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return AddressSignUpEntity
     */
    public function setCity(string $city): AddressSignUpEntity
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return AddressSignUpEntity
     */
    public function setStreet(string $street): AddressSignUpEntity
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return int
     */
    public function getHouseNumber(): int
    {
        return $this->houseNumber;
    }

    /**
     * @param int $houseNumber
     *
     * @return AddressSignUpEntity
     */
    public function setHouseNumber(int $houseNumber): AddressSignUpEntity
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }
}
