<?php

declare(strict_types=1);

namespace App\Entity;

class BillingSignUpEntity
{
    private string $owner;

    private string $iban;

    /**
     * @return string
     */
    public function getOwner(): string
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     *
     * @return BillingSignUpEntity
     */
    public function setOwner(string $owner): BillingSignUpEntity
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     *
     * @return BillingSignUpEntity
     */
    public function setIban(string $iban): BillingSignUpEntity
    {
        $this->iban = $iban;

        return $this;
    }
}
