<?php

declare(strict_types=1);

namespace App\Entity;

class GeneralSignUpEntity
{
    private string $firstName;

    private string $lastName;

    private string $phone;

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return GeneralSignUpEntity
     */
    public function setFirstName(string $firstName): GeneralSignUpEntity
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return GeneralSignUpEntity
     */
    public function setLastName(string $lastName): GeneralSignUpEntity
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return GeneralSignUpEntity
     */
    public function setPhone(string $phone): GeneralSignUpEntity
    {
        $this->phone = $phone;

        return $this;
    }
}
