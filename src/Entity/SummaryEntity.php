<?php

declare(strict_types=1);

namespace App\Entity;



class SummaryEntity
{
    private string $paymentDataId;

    public function getPaymentDataId(): string
    {
        return $this->paymentDataId;
    }

    public function setPaymentDataId(string $paymentDataId): SummaryEntity
    {
        $this->paymentDataId = $paymentDataId;
        return $this;
    }
}
