<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\AddressSignUpEntity;
use App\Entity\BillingSignUpEntity;
use App\Entity\GeneralSignUpEntity;
use App\FormType\AddressSignUpType;
use App\FormType\BillingSignUpType;
use App\FormType\GeneralSignUpType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class SignUpFormFactory
{
    private FormFactory $formFactory;

    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function createGeneralForm(): FormInterface
    {
        $entity = new GeneralSignUpEntity();

        return $this->formFactory->createBuilder(GeneralSignUpType::class, $entity)
            ->getForm();
    }

    public function createAddressForm(): FormInterface
    {
        $entity = new AddressSignUpEntity();

        return $this->formFactory->createBuilder(AddressSignUpType::class, $entity)
            ->getForm();
    }

    public function createBillingForm(): FormInterface
    {
        $entity = new BillingSignUpEntity();

        return $this->formFactory->createBuilder(BillingSignUpType::class, $entity)
            ->getForm();
    }
}
