<?php

declare(strict_types=1);

namespace App\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AddressSignUpType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $required = ['required' => true];

        $builder
            ->add('zipCode', NumberType::class, $required)
            ->add('city', TextType::class, $required)
            ->add('street', TextType::class, $required)
            ->add('houseNumber', NumberType::class, $required)
            ->add('submit', SubmitType::class, [
                'label' => 'Submit',
            ]);
    }
}
