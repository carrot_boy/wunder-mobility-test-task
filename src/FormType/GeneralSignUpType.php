<?php

declare(strict_types=1);

namespace App\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class GeneralSignUpType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $required = ['required' => true];

        $builder
            ->add('firstName', TextType::class, $required)
            ->add('lastName', TextType::class, $required)
            ->add('phone', TextType::class, $required)
            ->add('submit', SubmitType::class, [
                'label' => 'Submit',
            ]);
    }
}
