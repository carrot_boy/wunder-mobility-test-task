<?php

declare(strict_types=1);

namespace App\Repository;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SignUpRepository
{
    private SessionInterface $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function saveStep(string $step, $data): void
    {
        $this->session->set($step, $data);
    }

    public function saveCustomData(string $key, $value): void
    {
        $this->session->set($key, $value);
    }

    public function retrieveByKey(string $key)
    {
        return $this->session->get($key);
    }

    public function findDataForStep(string $step)
    {
        return $this->session->get($step);
    }
}
