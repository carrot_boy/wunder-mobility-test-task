<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\SummaryEntity;
use App\Repository\SignUpRepository;

class SignUpStateManager
{
    public const FIRST_STEP = 'signup.general';
    public const SECOND_STEP = 'signup.address';
    public const THIRD_STEP = 'signup.billing';
    public const FOURTH_STEP = 'signup.success';

    private const ROUTE_ORDER = [
        self::FIRST_STEP => self::SECOND_STEP,
        self::SECOND_STEP => self::THIRD_STEP,
        self::THIRD_STEP => self::FOURTH_STEP,
    ];

    private const SAVE_HANDLER_MAP = [
        self::THIRD_STEP => 'retrievePaymentId',
    ];

    private SignUpRepository $repository;

    private SignUpSaveHandler $saveHandler;

    public function __construct(SignUpRepository $repository, SignUpSaveHandler $saveHandler)
    {
        $this->repository = $repository;
        $this->saveHandler = $saveHandler;
    }

    public function saveState(string $route, $data, ?string $saveDataKey = null): void
    {
        $this->repository->saveStep($route, $data);
        if (array_key_exists($route, self::SAVE_HANDLER_MAP) && isset($saveDataKey)) {
            $method = self::SAVE_HANDLER_MAP[$route];
            $result = $this->saveHandler->$method($data);

            $this->repository->saveCustomData($saveDataKey, $result);
        }
    }

    public function isStepFilled(string $route): bool
    {
        $userDateForStep = $this->getDataForStep($route);

        return isset($userDateForStep);
    }

    public function getNextStep(string $route): string
    {
        if (array_key_exists($route, self::ROUTE_ORDER)) {
            return self::ROUTE_ORDER[$route];
        }

        return self::FIRST_STEP;
    }

    public function getNextPossibleStep(string $route): string
    {
        $userData = $this->getDataForStep($route);
        if (isset($userData)) {
            $nextStep = self::ROUTE_ORDER[$route];

            return $this->getNextPossibleStep($nextStep);
        }

        return $route;
    }

    public function getSummary(): SummaryEntity
    {
        $paymentDataId = $this->repository->retrieveByKey(SignUpSaveHandler::PAYMENT_ID_KEY);

        return (new SummaryEntity())
            ->setPaymentDataId($paymentDataId);
    }

    private function getDataForStep(string $route)
    {
        return $this->repository->findDataForStep($route);
    }
}
